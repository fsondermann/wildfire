from enum import Enum, IntEnum

class States(IntEnum):
    """States of a cell"""
    NoState = 0
    CITY = 1
    VEGETATION = 2
    BURNING = 3
    BURNED = 4