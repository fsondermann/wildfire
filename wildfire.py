import numpy as np

from states import States

def get_cells_around(matrix, i, j, borderless = True):
    """Helper function to get the indicies around a cell"""
    n_cells_x = matrix.shape[0]
    n_cells_y = matrix.shape[1]

    around = []
    for x in [-1, 0, 1]:
        for y in [-1, 0, 1]:
            i_ind = i+x
            j_ind = j+y
            if (i == n_cells_x - 1 and x == 1):
                i_ind = 0
            if (j == n_cells_y - 1 and y == 1):
                j_ind = 0
            if (borderless):
                around.append(np.array([i_ind, j_ind]))
            elif (not (
                (i_ind < 0) or 
                (i_ind == 0 and i == n_cells_x - 1) or 
                (j_ind < 0) or
                (j_ind == 0 and j == n_cells_y - 1)
                )):
                around.append(np.array([i_ind, j_ind]))
    return around

def get_angle_between(pos1, pos2):
    """Helper function to get the angle between 2 neighbor cells"""
    diff = pos2 - pos1
    for i in range(2):
        if (not diff[i] in [-1, 0, 1]):
            if (diff[i] < 0):
                diff[i] = 1
            else:
                diff[i] = -1
    angles = np.array([[0, 0, 180],
                [270, 315, 225],
                [90, 45, 135]])
    return angles[diff[0], diff[1]]

def calculate_p_burn(universe, burning, flamable, probs, wind):
    """Calculate p_burn probability a cell ignites a surrounding cell
    params:
        universe = {
            "states": matrix with states,
            "vegetation": matrix with vegetation type per cell,
            "density": matrix with density per cell
        },
        burning = np array position of burning cell,
        flamable = np array position of cell that could be ignited,
        probs = {
            "p_h": constant probability a cell starts to burn,
            "p_veg": array with probabilities for vegetation types,
            "p_den": array with probabilities for density types
        },
        wind = {
            "c1": c1 constant for wind probability,
            "c2": c2 constant for wind probability,
            "direction": wind direction,
            "speed": wind speed
        }
    returns probability flamable will be ignited
    """
    p_h = probs["p_h"]

    c1 = wind["c1"]
    c2 = wind["c2"]
    w_angle = wind["direction"]
    w_speed = wind["speed"]
    pos_angle = get_angle_between(burning, flamable)
    angle = w_angle - pos_angle

    # p_wind = exp(c1*V)*f_t
    # f_t = exp(V*c2(cos(phi) - 1))
    p_wind = np.exp(c1 * w_speed) * (np.exp(w_speed * c2 * (np.cos(np.deg2rad(angle)) - 1)))

    vegetation = universe["vegetation"]
    p_veg = probs["p_veg"][(int) (vegetation[flamable[0], flamable[1]] - 1)]

    density = universe["density"]
    p_den = probs["p_den"][(int) (density[flamable[0], flamable[1]] - 1)]

    # p_burn = p_h*(1+ veg)*(1 + den)*p_wind*p_slope
    p_burn = p_h * (1 + p_veg) * (1 + p_den) * p_wind
    return p_burn

def update_universe(universe, probs, wind, borderless = True):
    """Update universe
    params:
        universe = {
            "states": matrix with states,
            "vegetation": matrix with vegetation type per cell,
            "density": matrix with density per cell
        },
        probs = {
            "p_h": constant probability a cell starts to burn,
            "p_veg": array with probabilities for vegetation types,
            "p_den": array with probabilities for density types
        },
        wind = {
            "c1": c1 constant for wind probability,
            "c2": c2 constant for wind probability,
            "direction": wind direction,
            "speed": wind speed
        }
    returns universe, changed
    """

    # Get states, vegetation and density from universe
    states_matrix = universe["states"]
    vegetation = universe["vegetation"]
    density = universe["density"]

    # Universe size
    n_cells_x = states_matrix.shape[0]
    n_cells_y = states_matrix.shape[1]

    # Create new states matrix
    new_states = np.zeros((n_cells_x, n_cells_y))

    changed = False
    for i in range(n_cells_x):
        for j in range(n_cells_y):
            # Get current state
            state = States((int) (states_matrix[i, j]))
            # Get cells around current cell
            around = get_cells_around(states_matrix, i, j, borderless)

            # Get new state from new states matrix
            new_state = States((int) (new_states[i, j]))

            # Rule 1: city stays city
            if (state == States.CITY):
                new_state = States.CITY
            # Rule 2: burning gets burned and can ignite cells around
            elif (state == States.BURNING):
                new_state = States.BURNED

                # For all cells around burning cell
                for cell in around:
                    # If cell can be ignited
                    if (states_matrix[cell[0], cell[1]] == States.VEGETATION.value):
                        # Calculate p_burn and random value
                        rand = np.random.rand(1)[0]
                        p_burn = calculate_p_burn(universe, np.array([i, j]), cell, probs, wind)
                        if (rand < p_burn):
                            new_states[cell[0], cell[1]] = States.BURNING.value
                            changed = True
            # Rule 3: burned stays burned
            elif (state == States.BURNED):
                new_state = States.BURNED
            
            # If no rule was used the state stays the same
            if (new_state == 0):
                new_state = state
            
            # Set new state in new states matrix
            new_states[i, j] = new_state.value

            # Check if the state changed for the cell
            if (not state == new_state):
                changed = True
    
    # Create new universe
    new_universe = {
        "states": new_states,
        "vegetation": vegetation,
        "density": density
    }
    return new_universe, changed