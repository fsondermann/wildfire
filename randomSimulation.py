import numpy as np

import matplotlib as mpl
import matplotlib.pyplot as plt
import matplotlib.animation as animation
from matplotlib import colors

from randomUniverse import init_random_universe
from wildfire import update_universe
from states import States

def main(n_cells_x, n_cells_y, creatingParams, probs, wind, borderless, save_path, evolutions=0, figsize=None, delay_between_frames=100, delay_between_repeat=1000):
    """Simulate a random wildfire
    params:
        n_cells_x: int
        n_cells_y: int
        creatingParams = {
            "p_city": probability a cell is a city
            "vegetation": array, probabilities a vegetation cell has vegetation type
            "density": array, probabilities a vegetation cell has density type
        },
        probs = {
            "p_den": array, probabilities a density type changes probability
            "p_veg": array, probabilities a vegetation type changes probability
            "p_h": constant probability
        },
        wind = {
            "c1": c1 constant
            "c2": c2 constant
            "speed": speed value
            "direction": direction angle in degree
        },
        borderless: boolean,
        save_path: path animation will be stored,
        evolutions: maximum of time steps or 0 for no limit,
        figsize: figsize for plot,
        deleay_between_frames: in ms,
        delay_between_repeat: in ms
    """

    # Init Game of Life
    universe = init_random_universe(n_cells_x, n_cells_y, creatingParams)
    universe["states"][10:15, 10:15] = States.BURNING.value

    # Init Matplotlib Figure
    fig = plt.figure(figsize=figsize, frameon=False)

    # Color settings
    c_list = ['grey', 'green', 'red', 'black']
    cmap = colors.ListedColormap(c_list)
    bounds = [0.5, 1.5, 2.5, 3.5, 4.5]
    norm = colors.BoundaryNorm(bounds, cmap.N)

    # Display first universe as image
    #im = plt.imshow(presentation, animated=True, cmap = cmap, norm=norm)
    im = plt.imshow(universe["states"], animated=True, cmap = cmap, norm=norm)
    plt.axis('off')

    # List to store images and append first image to list
    ims = []
    ims.append([im])

    changed = True
    i = 0
    # for each evolution
    while(changed and (evolutions == 0 or i < evolutions)):

        i += 1
        # update current universe
        universe, changed = update_universe(universe, probs, wind, borderless)

        # get new image of current universe and append to image storage list
        im = plt.imshow(universe["states"], animated=True, cmap = cmap)
        plt.axis('off')
        ims.append([im])
    
    # repeat_delay does not work for me in animation
    # This adds the last frame to create a delay
    for j in range((int) (delay_between_repeat / delay_between_frames)):
        ims.append([ims[-1][0]])
    
    # perform animation based on imnages stored in ims list and put on fig object
    ani = animation.ArtistAnimation(fig=fig, artists=ims, interval=delay_between_frames)

    # save animation
    ani.save(save_path)

# Simulation
if __name__ == '__main__':

    n_cells_x   = 100
    n_cells_y   = 100
    creatingParams = {
        "p_city": 0.1,
        "vegetation": [0.2, 0.5, 0.3],
        "density": [0.2, 0.5, 0.3]
    }
    wind = {
        "c1": 0.0045,
        "c2": 0.131,
        "speed": 8,
        "direction": 350
    }
    probs = {
        "p_den": [-0.4, 0, 0.3],
        "p_veg": [-0.3, 0, 0.4],
        "p_h": 0.58
    }
    borderless = False
    save_path   = 'wildfire_Nx={}_Ny={}.gif'.format(n_cells_x, n_cells_y)   

    main(n_cells_x, n_cells_y, creatingParams, probs, wind, borderless, save_path)