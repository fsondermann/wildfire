import numpy as np

from states import States

# Helper function returns value from values list with probability from probs list
def get_random_value(values, probs):
    if (len(values) != len(probs)):
        return values[0]
    if (np.sum(probs) < 1):
        probs[-1] = 1 - np.sum(probs[:-1])
    if (np.sum(probs) > 1):
        return values[0]
    rand = np.random.rand(1)[0]
    for i in range(len(values)):
        sum = np.sum(probs[:i+1])
        if (rand < np.sum(probs[:i+1])):
            return values[i]
    return values[0]


# Creates random universe with size n_cells_x * n_cells_y
# creatingParams = {
#   "p_city": probability cell is city,
#   "vegetation": list of probabiltites for vegetation types
#   "density": list of probabilities for density
# }
#
# returns
# universe = {
#     "states": matrix with states,
#     "vegetation": matrix with vegetation type per cell,
#     "density": matrix with density per cell
# }
def init_random_universe(n_cells_x, n_cells_y, creatingParams = {
        "p_city": 0.2,
        "vegetation": [0.2, 0.4, 0.4],
        "density": [0.2, 0.4, 0.4]
    }):

    universe = {
        "states": np.zeros((n_cells_x, n_cells_y)),
        "vegetation": np.zeros((n_cells_x, n_cells_y)),
        "density": np.zeros((n_cells_x, n_cells_y))
    }

    p_city = creatingParams["p_city"]
    vegetation = creatingParams["vegetation"]
    density = creatingParams["density"]

    for i in range(n_cells_x):
        for j in range(n_cells_y):
            random = np.random.rand(1)

            if (random < p_city):
                universe["states"][i, j] = States.CITY.value
                universe["vegetation"][i, j] = 1
                universe["density"][i, j] = 1
            else:
                universe["states"][i, j] = States.VEGETATION.value
                universe["vegetation"][i, j] = get_random_value(np.arange(1, len(vegetation) + 1), vegetation)
                universe["density"][i, j] = get_random_value(np.arange(1, len(density) + 1), density)
    return universe